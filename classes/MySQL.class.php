<?php
/*
 *      Copyright 2014 Mario Rubiales <mariorubiales@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


class MYSQL {

	private static $instancia;
	private $server=NULL;
	private $bbddname=NULL;
	private $bbdduser=NULL;
	private $bbddpasswd=NULL;
	private $link=NULL;
	private $resultado;
	private $error;
	private $txterror;
	
/*
 * Constructor singleton de la clase, es privado para que no se pueda instanciar desde fuera.
 * Realiza una conexión con la BBDD.
 * @return objeto-conexión con la bbdd.
 */
	private function __construct(){
		$this->server=$_SESSION["config"]["mysql"]["server"];
		$this->bbddname=$_SESSION["config"]["mysql"]["dbname"];
		$this->bbdduser=$_SESSION["config"]["mysql"]["dbuser"];
		$this->bbddpasswd=$_SESSION["config"]["mysql"]["dbpasswd"];
		
		$this->link=@mysql_connect($this->server,$this->bbdduser,$this->bbddpasswd);
		if (!$this->link){
			die("<br/>Error de conexión con la bbdd:".mysql_error());
		}
		if (!@mysql_select_db($this->bbddname,$this->link)){
			die("<br/>Error al seleccionar la bbdd:".mysql_error());
		}
		return $this->link;
	}
/*
 * Método que retorna una instancia de la BBDD, la instancia sólo se creará la primera vez que se invoque
 * a dicho método, el resto de veces restornará siempre la misma instancia.
 * @return instancia de la propia clase.
 */
	public static function get_instancia(){
		if (self::$instancia===null)
			self::$instancia=new MYSQL();
		return self::$instancia;
	}

	/*
 * Método que realiza una consulta cualquiera contra la BBDD
 * @return objeto-resultado de la consulta.
 */

	public function get_resource($sql){
		$this->resultado=@mysql_query($sql,$this->link);
		if (!$this->resultado)
			die ("<br/>Error al ejecutar la consulta en la BBDD (MySQL):". mysql_error());
		return $this->resultado;
	}

/*
 * Método que realiza una consulta de actualización contra la BBDD,
 * especialemente pensado para los INSERT y UPDATE.
 * @sql Literal de la consulta SQL
 */
	public function set_resultados($sql){
		$this->resultado=@mysql_query($sql,$this->link);
		if (!$this->resultado)
			die ("<br/>Error al ejecutar actualización y/o inserción en la BBDD:". $sql);
	}

/*
 * Sobrecarga del método clone para impedir que clonen una clase singleton.
 */
	public function __clone(){
		trigger_error("ERROR: No vas a clonar una clase singleton",E_USER_ERROR);
	}
}

?>
