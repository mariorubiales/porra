<?php
/*
 *      ajaxhtml.php
 *      
 *      Copyright 2011 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
 
session_save_path(dirname(__FILE__)."/../tmp");
session_start();
  
require_once (dirname(__FILE__)."/../includes/html.inc.php");

switch ($_POST["opt"]){
	case 0:	echo get_html_groups();
				break;
	case 1:echo get_html_matches_phase2();
				break;
	case 2:	echo get_html_matches_phase3();
				break;
	case 3: 	echo get_html_matches_phase4();
				break;
	case 4: 	echo get_html_matches_phase5();
				break;
	case 5: 	echo get_html_champions();
				break;
	case 6:
				$vrappel=array();
				$vrappel["phase1"]=json_decode($_POST["phase1"]);
				$vrappel["phase2"]=json_decode($_POST["phase2"]);
				$vrappel["phase3"]=json_decode($_POST["phase3"]);
				$vrappel["phase4"]=json_decode($_POST["phase4"]);
				$vrappel["phase5"]=json_decode($_POST["phase5"]);
				$vrappel["phaseChampions"]=json_decode($_POST["phaseChampions"]);
				echo set_ticket_rappel($vrappel);
				break;
	case 7:	echo get_html_matches_phase1();
				break;
	case 8:	echo set_new_user($_POST["_name"],$_POST["_surname"]);
				break;
	case 9:	echo update_view_phases();
				break;	
	case 10:	$vticket=array();
				$vticket["phase1"]=json_decode($_POST["phase1"]);
				$vticket["phase2"]=json_decode($_POST["phase2"]);
				$vticket["phase3"]=json_decode($_POST["phase3"]);
				$vticket["phase4"]=json_decode($_POST["phase4"]);
				$vticket["phase5"]=json_decode($_POST["phase5"]);
				echo set_update_ticket($vticket,$_POST["champion"]);
				break;
	case 11:	echo get_html_fullticket($_POST["iduser"]);
				break;
	case 12:	echo get_html_defaulter();
				break;
	case 13:	echo update_user_state($_POST["sausageid"]);
				break;			
	case 14:	echo get_html_classifications();
				break;
	case 15:	if (isset ($_SESSION["admin"]))
					echo 1;
				else
					echo 0;	
				break;
	case 16:	echo get_html_auth();
				break;	
	case 17:	echo get_authentication($_POST["passwd"]);
				break;
	case 18:	echo get_html_current_userinfo();
				break;
	case 19: 	echo get_html_status();
				break;
}

?>

