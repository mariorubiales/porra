<?php
/*
 *      index.php
 *      
 *      Copyright 2014 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
 
session_save_path(dirname(__FILE__)."/tmp");
session_start();
if (!isset ($_SESSION["admin"]))
	header("Location: index.php");
require_once (getcwd()."/includes/global.inc.php");
require_once (getcwd()."/includes/html.inc.php");
require_once (getcwd()."/includes/html_update.inc.php");
load_app();
?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>porra 2018 (admin)</title>
<meta charset="utf-8" />
<link rel="stylesheet" href="css/site_admin.css" />
<link rel="icon" type="image/png" href="images/favicon.png" sizes="32x32" />
<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Philosopher" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script language="Javascript" src="libjs/jquery-1.11.0.min.js" type="text/javascript"></script>
<script language="Javascript" src="libjs/json2.js" type="text/javascript"></script>
<script language="Javascript" src="js/html_update.js" type="text/javascript"></script>
<script language="Javascript" src="js/resources.js" type="text/javascript"></script>
<script language="Javascript" type="text/javascript">
$(document).ready(function(){
 do_update_view_phases();
});
</script>	
</head>
<body>
    <header>
       <h1>Porra Mundial Rrasil 2018 (admin)</h1>
    </header>
    <section>
       <article>
        <?php echo get_html_groups();?>
        <div id="container_phase1">
            <h2>fase de grupos</h2>
            <div id="matches_phase1" class="matches">
                <?php echo get_html_update_phase1();?>
            </div>
        </div>
        <div id="container_phase2">
            <h2>octavos de final</h2>
            <div id="matches_phase2" class="matches">
						 <?php echo get_html_update_phase2();?>
            </div>
        </div>
        <div id="container_phase3">
            <h2>cuartos de final</h2>
            <div id="matches_phase3" class="matches">
						 <?php echo get_html_update_phase3();?>
            </div>
        </div>
        <div id="container_phase4">
            <h2>semifinales</h2>
            <div id="matches_phase4" class="matches">
						 <?php echo get_html_update_phase4();?>
						</div>
        </div>
        <div id="container_phase5">
            <h2>final</h2>
            <div id="matches_phase5" class="matches">
						 <?php echo get_html_update_phase5();?>						 
						</div>
        </div>
        <div id="container_champions">
            <h2>campeón y pichichi</h2>
            <div id="champions_results" class="matches">
						 <?php echo get_html_update_champions();?>
						</div>
        </div>
			<div class='button_bottom_right'><input id='btnupdate' type='button' value='actualizar'/></div>
			<div class='button_bottom_left'><input id='btncancel' type='button' value='<< volver'/></div>
       </article>
    </section>
    <aside>
    </aside>
    <footer><a href="https://twitter.com/antiliga">@antiliga</a></footer>
</body>
</html>
