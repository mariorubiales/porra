<?php
/*
 *      html.inc.php
 *      
 *      Copyright 2011 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once (dirname(__FILE__)."/global.inc.php");
require_once (dirname(__FILE__)."/resources.inc.php");

function get_html_status(){
  $oMYSQL=MYSQL::get_instancia();
  $sql="SELECT count(id) num FROM `participantes` WHERE `pagado`=1";
  $data_object=$oMYSQL->get_resource($sql);
  $data=mysql_fetch_object($data_object);
  $num=$data->num;
  $award=$data->num * $_SESSION["config"]["info"]["price"];
  $html=<<<eof
	<ul>
		<li>n&uacute;mero de participantes: ${num}</li>
		<li>bote: {$award} €</li>
	</ul>
eof;
	
	return $html;
	
}

function get_html_current_userinfo(){
	$fullname=utf8_decode($_SESSION["fullname"]);
	$registerdate=$_SESSION["registerdate"];
	$html=<<<eof
	<ul>
		<li>{$fullname}</li>
		<li>{$registerdate}</li>
	</ul>
eof;
return $html;
}

function get_html_classifications(){
  $oMYSQL=MYSQL::get_instancia();
  $sql="SELECT * FROM `participantes` WHERE `pagado`=1 AND `done`=1 ORDER BY `puntos` DESC, `apellidos`";
  $data_object=$oMYSQL->get_resource($sql);
	$fh = fopen(dirname(__FILE__)."/../".$_SESSION["config"]["info"]["last"], 'r+');
	$last=fread($fh,50);
  fclose($fh);
  $html=<<<eof
		<h2>Clasificaci&oacute;n</h2>
		<table>
			<tr>
				<th>pos</th>
				<th>participante</th>
				<th>aciertos</th>
				<th>puntos</th>
				<th>porra</th>
		</tr>
eof;
	 $pos=1;	
   while ($data=mysql_fetch_object($data_object)){
			$html .="<tr>\n<td>$pos</td>\n<td class='tdusername'>".ucwords(strtolower($data->nombre))." ".ucwords(strtolower($data->apellidos))."</td>";
			$html .="<td>$data->aciertos</td>\n<td>$data->puntos</td>";
			$html .="<td class='linkimg'><i class='material-icons md-30' id='zoom_$data->id' title='ver porra'>zoom_in</i></td>\n</tr>";
			$pos++;															
	 }
	 $html .="</table>";
	 $html .="<div class='info'>&uacute;ltima actualizaci&oacute;n: $last</div>";
	 return $html;
}

function get_html_defaulter(){

	$oMYSQL=MYSQL::get_instancia();
	$sql="SELECT * FROM `participantes` WHERE `pagado`=0 AND `done`=1 ORDER BY `apellidos`";
	$data_object=$oMYSQL->get_resource($sql);
	$title_html="<h2>Listado de morosos</h2>\n";
	$html="<table>\n";
	$pos=1;	
	 while ($data=mysql_fetch_object($data_object)){
			  $html .="<tr>\n<td class='pos'>$pos</td>\n<td class='tdusername'>".ucwords(strtolower($data->nombre))." ".ucwords(strtolower($data->apellidos))."</td>";
			  $html .="\n<td class='linkimg'><i class='material-icons md-30 success' id='ok_$data->id' title='marcar como pagado'>check_circle</i></td>\n<td class='linkimg'>";
			  $html .="<i class='material-icons md-30 delete' id='del_$data->id' title='eliminar participante'>delete</i></td>\n</tr>\n";
			  $pos++;															
	   }
	   $html .="</table>\n";

	  if ($pos==1) $html="<table><tr><td>No hay morosos ...</td></tr></table>";
	  return $title_html.$html;
  }

function get_html_listusers(){
  $oMYSQL=MYSQL::get_instancia();
  $sql="SELECT * FROM `participantes` WHERE `done`=1 ORDER BY `apellidos`";
  $data_object=$oMYSQL->get_resource($sql);
  $html="<h2>Participantes</h2>\n<table id='listusers'>\n";
  $pos=1;
	while ($data=mysql_fetch_object($data_object)){
			$class=($data->pagado==0)?"moroso":"";
			$html .="<tr>\n<td class='pos'>$pos</td>\n<td class='$class'>".ucwords(strtolower($data->nombre))." ".ucwords(strtolower($data->apellidos))."</td>";
			$html .="\n<td class='fecha'>$data->fecha</td>\n</tr>\n";
			$pos++;															
	 }
	 $html .="</table>\n";
	 $html .="<div class='info'>en gris los que a&uacute;n no han pagado</div>";	
	return $html;
}

function get_html_auth(){
	$html=<<<eof
		<h2>Autenticaci&oacute;n</h2>
		<table class="auth">
		<tr>
			<td><input type="password" id="authpasswd"/></td>
			<td><input type="button" value="aceptar" id="btnauth"/></td>
		</tr>
		</table>
eof;

return $html;
}
function get_html_groups(){
	 $oMYSQL=MYSQL::get_instancia();
   $sql="SELECT * FROM `selecciones` ORDER BY grupo ASC";
   $data_object=$oMYSQL->get_resource($sql);
   $html="";
   $group="X";
   $start=1;
   while ($data=mysql_fetch_object($data_object)){
		if (strcmp($group,$data->grupo)){
			if ($start)
				$start=0;
			else
				$html .="</ul></div>";
			$html .="<div class='groups'><p>Grupo $data->grupo</p><ul>";
		}
      $html .="<li>".utf8_encode($data->nombre)."</li>";
      $group=$data->grupo;
   }
   $html .="</ul></div>";

   return $html;

}

function get_html_matches_phase1(){
	$oMYSQL=MYSQL::get_instancia();
	$sql="SELECT id,ids1,ids2 FROM enfrentamientos order by id ASC";
	$data_object=$oMYSQL->get_resource($sql);
	$currentscore=$_SESSION["config"]["puntos"]["1x2"];
	$html=<<<eof
			<table>
				<tr>
					<th>Partido</th>
					<th>1X2</th>
					<th>Partido</th>
					<th>1X2</th>
					<th>Partido</th>
					<th>1X2</th>
					<th>Partido</th>
					<th>1X2</th>
				</tr>
eof;

	$vteams=$_SESSION["teams"];
	$cont=0;
	while ($data=mysql_fetch_object($data_object)){
		if ($cont==4){
			$html .="</tr>";
			$html .="<tr>";
			$cont=0;
		}
		$html .="<td>".utf8_encode($vteams[$data->ids1])." - ".utf8_encode($vteams[$data->ids2])."</td>";
		$html .="<td>".get_select_results($data->id)."</td>";
		$cont++;
	}
	
	$html .="</table>";
	$html .=<<<eof
	<div class="info">{$currentscore} punto por resultado acertado</div>
  <div class="button_bottom_right"><input id="btn2nextphase" type="button" value="continuar >>"/></div>
eof;

	return $html;
}

function get_html_matches_phase2(){
	$vselectgroups=get_select_phase2();
	$currentscore=$_SESSION["config"]["puntos"][2];
	$html=<<<eof
	<table id="tablephase2">
		<tr>
			<td>1</td>
			<td>1A-2B</td>
			<td>${vselectgroups["1A"]}</td>
			<td>${vselectgroups["2B"]}</td>
			<td>2</td>
			<td>1C-2D</td>
			<td>${vselectgroups["1C"]}</td>
			<td>${vselectgroups["2D"]}</td>
		</tr>
		<tr>
			<td>3</td>
			<td>1B-2A</td>
			<td>${vselectgroups["1B"]}</td>
			<td>${vselectgroups["2A"]}</td>
			<td>4</td>
			<td>1D-2C</td>
			<td>${vselectgroups["1D"]}</td>
			<td>${vselectgroups["2C"]}</td>
		</tr>
		<tr>
			<td>5</td>
			<td>1E-2F</td>
			<td>${vselectgroups["1E"]}</td>
			<td>${vselectgroups["2F"]}</td>
			<td>6</td>
			<td>1G-2H</td>
			<td>${vselectgroups["1G"]}</td>
			<td>${vselectgroups["2H"]}</td>
		</tr>
		<tr>
			<td>7</td>
			<td>1F-2E</td>
			<td>${vselectgroups["1F"]}</td>
			<td>${vselectgroups["2E"]}</td>
			<td>8</td>
			<td>1H-2G</td>
			<td>${vselectgroups["1H"]}</td>
			<td>${vselectgroups["2G"]}</td>
		</tr>
	</table>
	<div class="info">{$currentscore} puntos por resultado acertado</div>
eof;
	
	return $html;
}

function get_html_matches_phase3(){
	$currentscore=$_SESSION["config"]["puntos"][3];
	$html=<<<eof
	<table id="tablephase3">
		<tr>
			<td>C1</td>
			<td>1-2</td>
			<td id="tdwin1A2B"></td>
			<td id="tdwin1C2D"></td>
			<td>C2</td>
			<td>5-6</td>
			<td id="tdwin1E2F"></td>
			<td id="tdwin1G2H"></td>
		</tr>
		<tr>
			<td>C3</td>
			<td>3-4</td>
			<td id="tdwin1B2A"></td>
			<td id="tdwin1D2C"></td>
			<td>C4</td>
			<td>7-8</td>
			<td id="tdwin1F2E"></td>
			<td id="tdwin1H2G"></td>
		</tr>
	</table>
	<div class="info">{$currentscore} puntos por resultado acertado</div>
eof;
	
	return $html;
}

function get_html_matches_phase4(){
	$currentscore=$_SESSION["config"]["puntos"][4];
	$html=<<<eof
	<table id="tablephase4">
		<tr>
			<td class="tdphase4">I</td>
			<td class="tdphase4">C1-C2</td>
			<td id="tdwin12" class="tdphase4"></td>
			<td id="tdwin56" class="tdphase4"></td>
			<td>II</td>
			<td>C3-C4</td>
			<td id="tdwin34"></td>
			<td id="tdwin78"></td>
		</tr>
	</table>
	<div class="info">{$currentscore} puntos por resultado acertado</div>
eof;
	
	return $html;
}

function get_html_matches_phase5(){
	$currentscore=$_SESSION["config"]["puntos"][5];
	$html=<<<eof
	<table id="tablephase5">
		<tr>
			<td class="tdphase5">I-II</td>
			<td id="tdwinC1C2" class="tdphase4"></td>
			<td id="tdwinC3C4" class="tdphase4"></td>
		</tr>
	</table>
	<div class="info">{$currentscore} puntos por resultado acertado</div>
eof;
	
	return $html;
}

function get_html_champions(){
	$selecthtml=get_select_allteams();
	$currentscore=$_SESSION["config"]["puntos"][6];
	$html=<<<eof
	<table id="tablechampions">
		<tr>
			<th>campeón</th>
			<th>pichichi</th>
		</tr>
		<tr>
			<td id="tdwinC1C2">${selecthtml}</td>
			<td id="tdwinC3C4"><input type="text" id='pichichi' /></td>
		</tr>
	</table>
	<div class="info">{$currentscore} puntos por resultado acertado</div>
eof;
	
	return $html;
}

function get_html_fullticket($iduser){
	$oMYSQL=MYSQL::get_instancia();
	$v1x2=array();
	$vmatches=array();
	//nombre y apellidos del usuario que se está consultando
	$sql="SELECT * FROM `participantes` WHERE `id`='$iduser'";
	$data_object=$oMYSQL->get_resource($sql);
	$data=mysql_fetch_object($data_object);
	$fullname=ucwords(strtolower($data->nombre." ".$data->apellidos));
	
	$sql="SELECT `idenfrentamiento`,`resultado` FROM `boleto` WHERE `idparticipante`='$iduser'";
	$data_object=$oMYSQL->get_resource($sql);
	while ($data=mysql_fetch_object($data_object)){
		if ($data->resultado==3)
			$v1x2[$data->idenfrentamiento]="X";
		else
			$v1x2[$data->idenfrentamiento]=$data->resultado;
	}	
	
	unset($_SESSION["boleto"]);
	$_SESSION["boleto"]=$v1x2;
	
	$sql="SELECT `id`,`ids1`,`ids2` FROM `enfrentamientos`";
	$data_object=$oMYSQL->get_resource($sql);
	
	//1X2
	$currentscore=$_SESSION["config"]["puntos"]["1x2"];
	$html=<<<eof
	<div id="container_ticket1">
	<div id="ticketowner">{$fullname}</div>
		<h2>Fase de grupos</h2>
			<div id="ticket_phase1" class="matches">
				<table id='ticket1X2' class="tickets">
					<tr>
						<th>partido</th>
						<th>1X2</th>
						<th>partido</th>
						<th>1X2</th>
						<th>partido</th>
						<th>1X2</th>
						<th>partido</th>
						<th>1X2</th>
					</tr>
eof;
			
	$vteams=$_SESSION["teams"];
	$cont=0;
	while ($data=mysql_fetch_object($data_object)){
		if ($cont==4){
			$html .="</tr>\n";
			$html .="<tr>\n";
			$cont=0;
		}
		$html .="<td>".utf8_encode($vteams[$data->ids1])." - ".utf8_encode($vteams[$data->ids2])."</td>\n";
		$html .="<td>".$_SESSION["boleto"][$data->id]."</td>\n";
		$cont++;
	}
	
	$html .="</table>\n</div>\n</div>";
	$html .="<div class='info infophase'>$currentscore punto por resultado acertado</div>";
	
	$sql="SELECT `idseleccion`,`idfase`,`idpos` FROM `boletofases` WHERE `idparticipante`='$iduser' ORDER BY `idfase`";
	$data_object=$oMYSQL->get_resource($sql);
	$currentphase=2;
	$vticketphases=array();
	$vticket=array();
	while ($data=mysql_fetch_object($data_object)){
		if ($currentphase!=$data->idfase){
			$vticketphases[$currentphase]=$vticket;
			$currentphase=$data->idfase;
			$vticket=array();
		}	
		$vticket[$data->idpos]=utf8_encode($_SESSION["teams"][$data->idseleccion]);
	}
	$vticketphases[$currentphase]=$vticket;
	
	//OCTAVOS
	$vticket=array();
	$vticket=$vticketphases[2];
	$currentscore=$_SESSION["config"]["puntos"][2];
	$html .=<<<eof
	<div id="container_ticket2">
		<h2>Octavos de final</h2>
      <div id="ticket_phase2" class="matches">
			<table id="tableticketphase2" class="tickets">
				<tr>
					<td>1</td>
					<td>1A-2B</td>
					<td>${vticket["1A"]}</td>
					<td>${vticket["2B"]}</td>
					<td>2</td>
					<td>1C-2D</td>
					<td>${vticket["1C"]}</td>
					<td>${vticket["2D"]}</td>
				</tr>
				<tr>
					<td>3</td>
					<td>1B-2A</td>
					<td>${vticket["1B"]}</td>
					<td>${vticket["2A"]}</td>
					<td>4</td>
					<td>1D-2C</td>
					<td>${vticket["1D"]}</td>
					<td>${vticket["2C"]}</td>
				</tr>
				<tr>
					<td>5</td>
					<td>1E-2F</td>
					<td>${vticket["1E"]}</td>
					<td>${vticket["2F"]}</td>
					<td>6</td>
					<td>1G-2H</td>
					<td>${vticket["1G"]}</td>
					<td>${vticket["2H"]}</td>
				</tr>
				<tr>
					<td>7</td>
					<td>1F-2E</td>
					<td>${vticket["1F"]}</td>
					<td>${vticket["2E"]}</td>
					<td>8</td>
					<td>1H-2G</td>
					<td>${vticket["1H"]}</td>
					<td>${vticket["2G"]}</td>
				</tr>
			</table>	
		</div>
	</div>
	<div class="info infophase">{$currentscore} puntos por resultado acertado</div>
eof;
	
	//CUARTOS
	$vticket=array();
	$vticket=$vticketphases[3];
	$currentscore=$_SESSION["config"]["puntos"][3];
	$html .=<<<eof
	<div id="container_ticket3">
		<h2>Cuartos de final</h2>
      <div id="ticket_phase3" class="matches">
			<table id="tableticketphase3" class="tickets">
				<tr>
					<td>C1</td>
					<td>1-2</td>
					<td id="tdwin1A2B">${vticket["win1A2B"]}</td>
					<td id="tdwin1C2D">${vticket["win1C2D"]}</td>
					<td>C2</td>
					<td>5-6</td>
					<td id="tdwin1E2F">${vticket["win1E2F"]}</td>
					<td id="tdwin1G2H">${vticket["win1G2H"]}</td>
				</tr>
				<tr>
					<td>C3</td>
					<td>3-4</td>
					<td id="tdwin1B2A">${vticket["win1B2A"]}</td>
					<td id="tdwin1D2C">${vticket["win1D2C"]}</td>
					<td>C4</td>
					<td>7-8</td>
					<td id="tdwin1F2E">${vticket["win1F2E"]}</td>
					<td id="tdwin1H2G">${vticket["win1H2G"]}</td>
				</tr>
			</table>	
		</div>
	</div>
	<div class="info infophase">{$currentscore} puntos por resultado acertado</div>
eof;
	
	//SEMIFINALES
	$vticket=array();
	$vticket=$vticketphases[4];
	$currentscore=$_SESSION["config"]["puntos"][4];
	$html .=<<<eof
	<div id="container_ticket4">
		<h2>Semifinales</h2>
      <div id="ticket_phase4" class="matches">
			<table id="tableticketphase4" class="tickets">
				<tr>
					<td class="tdticketphase4">I</td>
					<td class="tdticketphase4">C1-C2</td>
					<td id="tdwin12" class="tdticketphase4">${vticket["win12"]}</td>
					<td id="tdwin56" class="tdticketphase4">${vticket["win56"]}</td>
					<td>II</td>
					<td>C3-C4</td>
					<td id="tdwin34">${vticket["win34"]}</td>
					<td id="tdwin78">${vticket["win78"]}</td>
				</tr>
			</table>	
		</div>
	</div>
	<div class="info infophase">{$currentscore} puntos por resultado acertado</div>
eof;

	//FINAL
	$vticket=array();
	$vticket=$vticketphases[5];
	$currentscore=$_SESSION["config"]["puntos"][5];
	$html .=<<<eof
	<div id="container_ticket5">
		<h2>Final</h2>
      <div id="ticket_phase5" class="matches">
			<table id="tableticketphase5" class="tickets">
				<tr>
					<td class="tdphase5">I-II</td>
					<td id="tdwinC1C2" class="tdticketphase4">${vticket["winC1C2"]}</td>
					<td id="tdwinC3C4" class="tdticketphase4">${vticket["winC3C4"]}</td>
				</tr>
			</table>	
		</div>
	</div>
	<div class="info infophase">{$currentscore} puntos por resultado acertado</div>
eof;
	
	//CAMPEON Y PICHICHI
	$vticket=array();
	$vticket=$vticketphases[6];
	$sql="SELECT `nombre` FROM `pichichi` WHERE `idparticipante`='$iduser'";
	$data_object=$oMYSQL->get_resource($sql);
	$data=mysql_fetch_object($data_object);
	$pichichitxt=ucwords(strtolower(utf8_encode($data->nombre)));
	
	$currentscore=$_SESSION["config"]["puntos"][6];
	$html .=<<<eof
	<div id="container_ticketchampions">
		<h2>campeón y pichichi</h2>
      <div id="ticket_champions_results" class="matches">
			<table id="tableticketchampions" class="tickets">
				<tr>
					<th>Campeón</th>
					<th>Pichichi</th>
				</tr>
				<tr>
					<td id="tdwinC1C2">${vticket["1"]}</td>
					<td id="tdwinC3C4">${pichichitxt}</td>
				</tr>
			</table>	
		</div>
	</div>
	<div class="info infophase">{$currentscore} puntos por resultado acertado</div>
eof;
	return $html;
}

?>
