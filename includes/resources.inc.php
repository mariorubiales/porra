<?php

/*  resources.inc.php
 *
 *  Copyright (C) 2014  Mario Rubiales Gómez <mariorubiales@gmail.com>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function set_new_user($name,$surname){
	$iduser=md5($name.$surname.time());
	
	$oMYSQL=MYSQL::get_instancia();
	$now=date("d M - H:i");
	$sql="INSERT INTO `participantes` (id,nombre,apellidos,puntos,pagado,aciertos,fallos,fecha,done) VALUES ";
	$sql .="('$iduser','$name','$surname',0,0,0,0,'$now',0);";
	$data_object=$oMYSQL->set_resultados($sql);
	$_SESSION["iduser"]=$iduser;
	$_SESSION["fullname"]=ucwords(strtolower(utf8_encode($name." ".$surname)));
	$_SESSION["registerdate"]=$now;
	return $iduser;
}

function get_authentication($passwd){
	$done=0;
	if (!strcmp($passwd,$_SESSION["config"]["admin"]["passwd"])){
		$_SESSION["admin"]="comeon";
		$done=1;
	}
	
	return $done;
}

function update_user_state($sausageid){
	$oMYSQL=MYSQL::get_instancia();
	$vaction=array();
	$vaction=explode("_",$sausageid);

	switch ($vaction[0]){
		case "del": $sql="DELETE FROM `participantes` WHERE `id`='".$vaction[1]."';\n";
						$oMYSQL->set_resultados($sql);
						$sql="DELETE FROM `boleto` WHERE `idparticipante`='".$vaction[1]."';\n";
						$oMYSQL->set_resultados($sql);
						$sql="DELETE FROM `boletofases` WHERE `idparticipante`='".$vaction[1]."';\n";
						$oMYSQL->set_resultados($sql);
						$sql="DELETE FROM `pichichi` WHERE `idparticipante`='".$vaction[1]."';\n";
						$oMYSQL->set_resultados($sql);
						break;
		case "ok": $sql="UPDATE `participantes` SET `pagado`=1 WHERE `id`='".$vaction[1]."';";
						$oMYSQL->set_resultados($sql);
						break;
				
	}
}

function get_select_results($idmatch){
	$html=<<<eof
	<select id='1X2#$idmatch' class='select1x2'>
	<option value="0">...</option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">X</option>
	</select>
eof;
 
	return $html;
}

function get_select_update_results($idmatch,$value){
	$html=<<<eof
	<select id='1X2#$idmatch' class='select1x2'>
	<option value="{$value}" selected>...</option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">X</option>
	</select>
eof;
 
	return $html;
}

function get_select_phase2(){
	$vselects=array();
	$optionhtml="";
	
	foreach ($_SESSION["groups"] as $key => $value){
		foreach ($value as $teamdirtyname){
			$teamname=utf8_encode($teamdirtyname);
			$idteam=array_search($teamdirtyname, $_SESSION["teams"]);
			$optionhtml .="<option value='$idteam'>$teamname</option>";
		}
		$optionhtml .="</select>";
		$selecthtml="<select id='1$key'><option value='0' selected>1 &ordm; grupo $key</option>";
		$vselects["1".$key]=$selecthtml.$optionhtml;
		$selecthtml="<select id='2$key'><option value='0' selected>2 &ordm; grupo $key</option>";
		$vselects["2".$key]=$selecthtml.$optionhtml;
		$optionhtml="";
	}
	
	return $vselects;
  
}    

function get_select_phase3(){
	$vselects=array();
	$optionhtml="";
	foreach ($_SESSION["groups"]["A"] as $teamdirtyname){
		$teamname=utf8_encode($teamdirtyname);
		$idteam=array_search($teamdirtyname, $_SESSION["teams"]);
		$optionhtml .="<option value='$idteam'>$teamname</option>";
	}
		$optionhtml .="</select>";
		$selecthtml="<select id='1-2'><option value='0'>ganador 1-2</option>";
		$vselects["1".$key]=$selecthtml.$optionhtml;
		$selecthtml="<select id='2$key'><option value='0'>2 &ordm; grupo $key</option>";
		$vselects["2".$key]=$selecthtml.$optionhtml;
		$optionhtml="";
	
	return $vselects;
  
}

function get_select_allteams(){
  
  $html="<select id='campeon'><option value='0' selected>...</option>";
  foreach ($_SESSION["teams"] as $key => $value)
    $html .="<option value='$key'>".utf8_encode($value)."</option>";
  
  $html .="</select>";
  
  return $html;

}

function set_update_ticket($vticket,$idcampeon){
	$oMYSQL=MYSQL::get_instancia();

	//LIMPIAMOS
	$cleansql="UPDATE `enfrentamientos` SET `resultado`=0";
	$oMYSQL->set_resultados($cleansql);
	$cleansql="TRUNCATE TABLE `fases`";
	$oMYSQL->set_resultados($cleansql);
	
	//ACTUALIZAR 1X2
	$sql="UPDATE `enfrentamientos`\nSET `resultado` = CASE\n";
	foreach ($vticket["phase1"] as $key=>$value){
		$vkey=explode("#",$key);
		$sql .="WHEN `id`=".$vkey[1]." THEN ".$value."\n";

	}
	$sql .="ELSE 0\nEND;";

	$oMYSQL->set_resultados($sql);
	
	//ACTUALIZAR FASES
	$sql="INSERT INTO `fases` (idseleccion,idfase,idpos) VALUES ";
	
	foreach ($vticket["phase2"] as $key=>$value)
		$sql .="($value,2,'$key'),";
	foreach ($vticket["phase3"] as $key=>$value)
		$sql .="($value,3,'$key'),";
	foreach ($vticket["phase4"] as $key=>$value)
		$sql .="($value,4,'$key'),";
	foreach ($vticket["phase5"] as $key=>$value)
		$sql .="($value,5,'$key'),";
		
	$sql .="($idcampeon,6,'1');";
	$oMYSQL->set_resultados($sql);
	
	update_classifications();
}

function update_classifications(){
	$oMYSQL=MYSQL::get_instancia();
	$venfrentamientos=array();
	$vparticipantes=array();
	$vboleto=array();
	$vboletomain=array();
	
	//pasamos los boletos de los participantes a una array del tipo: 
	/*Array
	(
    [91ad8b035770206aaaab3d61e7a08b81] => Array
        (
            [1] => 1 
            [2] => 1
             .
             . 
   */
  $sql="SELECT `id` FROM `participantes` WHERE `done`=1";	
	$data_object=$oMYSQL->get_resource($sql);
	while ($data=mysql_fetch_object($data_object))
		array_push($vparticipantes,$data->id);//vector de id de participantes
		
	$sql="SELECT `idparticipante`,`idenfrentamiento`,`resultado` FROM `boleto`";
	$data_object=$oMYSQL->get_resource($sql);
	while ($data=mysql_fetch_object($data_object)){
		if (!isset($currentuser))
			$currentuser=$data->idparticipante;
		if (strcmp($currentuser,$data->idparticipante)){
			$vboletomain[$currentuser]=$vboleto;
			$vboleto=array();
			$currentuser=$data->idparticipante;
		}
		$vboleto[$data->idenfrentamiento]=$data->resultado;
	}
	$vboletomain[$currentuser]=$vboleto;
   
	$sql="SELECT `id`,`resultado` FROM `enfrentamientos`";	
	$data_object=$oMYSQL->get_resource($sql);
	while ($data=mysql_fetch_object($data_object))
		$venfrentamientos[$data->id]=$data->resultado;//vector de resultados reales
  	
  //1X2
  $vscoremain=array();
  $vscore=array();
  $phasepoints=$_SESSION["config"]["puntos"]["1x2"];
  foreach ($vparticipantes as $iduser){
		$hits=$total=0;
		foreach ($vboletomain[$iduser] as $key=>$value){
			if ($venfrentamientos[$key]==$value){
				$hits++;
				$total +=$phasepoints;
			}
		 $vscore["hits"]=$hits;
		 $vscore["total"]=$total;
		 $vscoremain[$iduser]=$vscore;
		 $vscore=array();
		}
	}
	//RESTO DE FASES
	$vphases=array(2,3,4,5,6);
	foreach ($vparticipantes as $iduser){
		foreach ($vphases as $idphase){
			$phasepoints=$_SESSION["config"]["puntos"][$idphase];
			$sql="SELECT count(id) hits FROM `boletofases` WHERE `idparticipante`='$iduser' and `idfase`=$idphase";
			$sql.=" AND `idseleccion` IN (SELECT `idseleccion` FROM `fases` WHERE `idfase`=$idphase);\n";
			$data_object=$oMYSQL->get_resource($sql);
			$data=mysql_fetch_object($data_object);
			$vscoremain[$iduser]["hits"] +=$data->hits;
			$vscoremain[$iduser]["total"] +=$data->hits * $phasepoints;
		}
	}
	$_SESSION["vscoremain"]=$vscoremain;

	//PASAMOS LAS PUNTUACIONES A LA BBDD
	foreach ($vparticipantes as $iduser){
		$sql="UPDATE `participantes` SET `puntos`=".$vscoremain[$iduser]["total"].", `aciertos`=".$vscoremain[$iduser]["hits"];
		$sql .=" WHERE `id`='$iduser'";
		$oMYSQL->set_resultados($sql);
		echo $sql;
	}
  
  //FECHA Y HORA DE ACTUALIZACION
	$now=date("d M - H:i");
	$fh = fopen(dirname(__FILE__)."/../".$_SESSION["config"]["info"]["last"], 'r+');
  	fwrite($fh,$now);
  	fclose($fh);
}

function set_ticket_rappel($vrappel){
	$oMYSQL=MYSQL::get_instancia();
	$iduser=$_SESSION["iduser"];
	
	//1X2
	$cleansql="DELETE FROM `boleto` WHERE `idparticipante`='$iduser'";
	$sql="INSERT INTO `boleto` (idparticipante,idenfrentamiento,resultado) VALUES ";
	foreach ($vrappel["phase1"] as $key => $value){
		$vkey=explode("#",$key);
		$sql .="('$iduser',$vkey[1],$value),";
	}
	$sql=substr($sql,0,-1).";";
	$oMYSQL->set_resultados($cleansql);
	$oMYSQL->set_resultados($sql);
	
	//OCTAVOS
	$sql="INSERT INTO `boletofases` (idparticipante,idseleccion,idfase,idpos) VALUES ";
	foreach ($vrappel["phase2"] as $key => $value)
		$sql .="('$iduser',$value,2,'$key'),";
	
	//CUARTOS
	foreach ($vrappel["phase3"] as $key => $value)
		$sql .="('$iduser',$value,3,'$key'),";

	//SEMIFINAL
	foreach ($vrappel["phase4"] as $key => $value)
		$sql .="('$iduser',$value,4,'$key'),";

	//FINAL
	foreach ($vrappel["phase5"] as $key => $value)
		$sql .="('$iduser',$value,5,'$key'),";
	
	//CAMPEON
	$idchampion=$vrappel["phaseChampions"]->{"campeon"};
	$sql .="('$iduser',$idchampion,6,'1');";
		
	$cleansql="DELETE FROM `boletofases` WHERE `idparticipante`='$iduser'";
	$oMYSQL->set_resultados($cleansql);
	$oMYSQL->set_resultados($sql);
	
	$cleansql="DELETE FROM `pichichi` WHERE `idparticipante`='$iduser'";
	$oMYSQL->set_resultados($cleansql);
	$pichichi=$vrappel["phaseChampions"]->{"pichichi"};
	$sql="INSERT INTO `pichichi` SET `idparticipante`='$iduser', `nombre`='$pichichi';";
	$oMYSQL->set_resultados($sql);
	
	//ACTIVAMOS EL CAMPO `done` DEL PARTICIPANTE A 1, DE ESTA FORMA SABEMOS QUE HA COMPLETADO EL BOLETO
	$sql="UPDATE `participantes` SET `done`=1 WHERE `id`='$iduser'";	
	$oMYSQL->set_resultados($sql);
	
	return get_html_fullticket($iduser);
}

function get_html_options_team_by_group(){
	foreach ($_SESSION["groups"] as $groupname=>$vgroups){
		$optionhtml="";
		foreach ($vgroups as $teamdirtyname){
				$idteam=array_search($teamdirtyname, $_SESSION["teams"]);
				$teamname=utf8_encode($teamdirtyname);
				$optionhtml .="<option value='$idteam'>$teamname</option>\n";
		}
		$vreturn[$groupname]=$optionhtml;
	}
	return $vreturn;
}

function get_html_selects_team_by_several_groups_old($id,$vgroups){
	$selecthtml="<select id='id'>\n";
	foreach ($vgroups as $group){
		foreach ($_SESSION["groups"][$group] as $teamdirtyname){
				$idteam=array_search($teamdirtyname, $_SESSION["teams"]);
				$teamname=utf8_encode($teamdirtyname);
				$selecthtml .="<option value='$idteam'>$teamname</option>\n";
		}
	}
	$selecthtml .="</select>\n";
	return $selecthtml;
}


/********************** UPDATE ***************************/

function update_view_phases(){
	$oMYSQL=MYSQL::get_instancia();
	$sql="SELECT `id`, `resultado` FROM `enfrentamientos`";
	$vresults=array();
	$data_object=$oMYSQL->get_resource($sql);
   while ($data=mysql_fetch_object($data_object))
		$vresults["1X2#".$data->id]=$data->resultado;
	
	$sql="SELECT `idseleccion`, `idpos` FROM `fases`";
	$data_object=$oMYSQL->get_resource($sql);
   while ($data=mysql_fetch_object($data_object))
		$vresults[$data->idpos]=$data->idseleccion;
			
	return json_encode($vresults);	
   
}

function update_view_phase($idphase){
	$oMYSQL=MYSQL::get_instancia();
	$sql="SELECT `idseleccion`, `idpos` FROM `fases` WHERE `idfase`=$idphase";
	$vresults=array();
	$data_object=$oMYSQL->get_resource($sql);
   while ($data=mysql_fetch_object($data_object))
		$vresults[$data->idpos]=$data->idseleccion;
		
	return json_encode($vresults);	
   
}

?>   
