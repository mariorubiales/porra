<?php
/*
 *      html_update.inc.php
 *      
 *      Copyright 2011 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

require_once (dirname(__FILE__)."/global.inc.php");
require_once (dirname(__FILE__)."/resources.inc.php");

function get_html_update_phase1(){
	$oMYSQL=MYSQL::get_instancia();
	$sql="SELECT id,ids1,ids2,resultado FROM enfrentamientos order by id ASC";
	$data_object=$oMYSQL->get_resource($sql);
	$html=<<<eof
			<table>
				<tr>
					<th>partido</th>
					<th>1X2</th>
					<th>partido</th>
					<th>1X2</th>
					<th>partido</th>
					<th>1X2</th>
					<th>partido</th>
					<th>1X2</th>
				</tr>
eof;

	$vteams=$_SESSION["teams"];
	$cont=0;
	while ($data=mysql_fetch_object($data_object)){
		if ($cont==4){
			$html .="</tr>";
			$html .="<tr>";
			$cont=0;
		}
		$html .="<td>".utf8_encode($vteams[$data->ids1])." - ".utf8_encode($vteams[$data->ids2])."</td>";
		$html .="<td>".get_select_results($data->id)."</td>";
		$cont++;
	}
	
	$html .="</table>";
	$html .=<<<eof
eof;

	return $html;
}

function get_html_update_phase2(){
	$vselectgroups=get_select_phase2();
	$html=<<<eof
	<table id="tablephase2">
		<tr>
			<td>1</td>
			<td>1A-2B</td>
			<td>${vselectgroups["1A"]}</td>
			<td>${vselectgroups["2B"]}</td>
			<td>2</td>
			<td>1C-2D</td>
			<td>${vselectgroups["1C"]}</td>
			<td>${vselectgroups["2D"]}</td>
		</tr>
		<tr>
			<td>3</td>
			<td>1B-2A</td>
			<td>${vselectgroups["1B"]}</td>
			<td>${vselectgroups["2A"]}</td>
			<td>4</td>
			<td>1D-2C</td>
			<td>${vselectgroups["1D"]}</td>
			<td>${vselectgroups["2C"]}</td>
		</tr>
		<tr>
			<td>5</td>
			<td>1E-2F</td>
			<td>${vselectgroups["1E"]}</td>
			<td>${vselectgroups["2F"]}</td>
			<td>6</td>
			<td>1G-2H</td>
			<td>${vselectgroups["1G"]}</td>
			<td>${vselectgroups["2H"]}</td>
		</tr>
		<tr>
			<td>7</td>
			<td>1F-2E</td>
			<td>${vselectgroups["1F"]}</td>
			<td>${vselectgroups["2E"]}</td>
			<td>8</td>
			<td>1H-2G</td>
			<td>${vselectgroups["1H"]}</td>
			<td>${vselectgroups["2G"]}</td>
		</tr>
	</table>	
eof;
	
	return $html;
}

function get_html_update_phase3(){
	$voptions=get_html_options_team_by_group();
	
	$html=<<<eof
	<table id="tablephase3">
		<tr>
			<td>C1</td>
			<td>1-2</td>
			<td id="tdwin1A2B">
				<select id="win1A2B">
					<option value="0" selected>ganador 1A-2B</option>
					${voptions["A"]}
					${voptions["B"]}
				</select>
			</td>
			<td id="tdwin1C2D">
				<select id="win1C2D">
					<option value="0" selected>ganador 1B-2A</option>
					${voptions["C"]}
					${voptions["D"]}
				</select>	
			</td>
			<td>C2</td>
			<td>5-6</td>
			<td id="tdwin1E2F">
				<select id="win1E2F">
					<option value="0" selected>ganador 1E-2F</option>
					${voptions["E"]}
					${voptions["F"]}
				</select>
			</td>
			<td id="tdwin1G2H">
				<select id="win1G2H">
					<option value="0" selected>ganador 1G-2H</option>
					${voptions["G"]}
					${voptions["H"]}
				</select>	
			</td>
		</tr>
		<tr>
			<td>C3</td>
			<td>3-4</td>
			<td id="tdwin1B2A">
				<select id="win1B2A">
					<option value="0" selected>ganador 1B-2A</option>
					${voptions["B"]}
					${voptions["A"]}
				</select>						
			</td>
			<td id="tdwin1D2C">
				<select id="win1D2C">
					<option value="0" selected>ganador 1D-2C</option>
					${voptions["C"]}
					${voptions["D"]}
				</select>						
			</td>
			<td>C4</td>
			<td>7-8</td>
			<td id="tdwin1F2E">
				<select id="win1F2E">
					<option value="0" selected>ganador 1F-2E</option>
					${voptions["F"]}
					${voptions["E"]}
				</select>						
			</td>
			<td id="tdwin1H2G">
				<select id="win1H2G">
					<option value="0" selected>ganador 1H-2G</option>
					${voptions["H"]}
					${voptions["G"]}
				</select>						
			</td>
		</tr>
	</table>	
eof;
	return $html;
}

function get_html_update_phase4(){
	$voptions=get_html_options_team_by_group();
	
	$html=<<<eof
	<table id="tablephase4">
		<tr>
			<td class="tdphase4">I</td>
			<td class="tdphase4">C1-C2</td>
			<td id="tdwin12" class="tdphase4">
				<select id="win12">
					<option value="0" selected>ganador 1-2</option>
					${voptions["A"]}
					${voptions["B"]}
					${voptions["C"]}
					${voptions["D"]}
				</select>	
			</td>
			<td id="tdwin56" class="tdphase4">
				<select id="win56">
					<option value="0" selected>ganador 5-6</option>
					${voptions["E"]}
					${voptions["F"]}
					${voptions["G"]}
					${voptions["H"]}
				</select>				
			</td>
			<td>II</td>
			<td>C3-C4</td>
			<td id="tdwin34">
				<select id="win34">
					<option value="0" selected>ganador 3-4</option>
					${voptions["A"]}
					${voptions["B"]}
					${voptions["C"]}
					${voptions["D"]}
				</select>				
			</td>
			<td id="tdwin78">
				<select id="win78">
					<option value="0" selected>ganador 7-8</option>
					${voptions["E"]}
					${voptions["F"]}
					${voptions["G"]}
					${voptions["H"]}
				</select>				
			</td>
		</tr>
	</table>	
eof;
	
	return $html;
}

function get_html_update_phase5(){
	$voptions=get_html_options_team_by_group();
	$html=<<<eof
	<table id="tablephase5">
		<tr>
			<td class="tdphase5">I-II</td>
			<td id="tdwinC1C2" class="tdphase4">
				<select id="winC1C2">
					<option value="0" selected>ganador C1-C2</option>
					${voptions["A"]}
					${voptions["B"]}
					${voptions["C"]}
					${voptions["D"]}
					${voptions["E"]}
					${voptions["F"]}
					${voptions["G"]}
					${voptions["H"]}
				</select>					
			</td>
			<td id="tdwinC3C4" class="tdphase4">
				<select id="winC3C4">
					<option value="0" selected>ganador C3-C4</option>
					${voptions["A"]}
					${voptions["B"]}
					${voptions["C"]}
					${voptions["D"]}
					${voptions["E"]}
					${voptions["F"]}
					${voptions["G"]}
					${voptions["H"]}
				</select>								
			</td>
		</tr>
	</table>	
eof;
	
	return $html;
}

function get_html_update_champions(){
	$selecthtml=get_select_allteams();
	
	$html=<<<eof
	<table id="tablechampions">
		<tr>
			<th>campeón</th>
			<th>pichichi</th>
		</tr>
		<tr>
			<td id="tdwinC1C2">${selecthtml}</td>
			<td id="tdwinC3C4"><input type="text" id='pichichi' /></td>
		</tr>
	</table>	
eof;
	
	return $html;
}

?>
