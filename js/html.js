/*
 *      html.js
 *      
 *      Copyright 2014 Mario Rubiales <mario@deckard>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

function do_set_register(name,surname){
  $("#loading").show();
  var container_html=get_html_container_phases();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 8,_name:name, _surname:surname},
          success:function(html){
            $("section article div#container_register").remove();
            $("section article").html(container_html);
            $("section article div#container_phase2, div#container_phase3,div#container_phase4,div#container_phase5,div#container_champions").hide();
            do_get_html_current_userinfo();
            do_get_html_allgroups();
            do_get_html_phase1();
            $("#loading").hide();
          }
  });
}

function do_get_html_auth(optmenu){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 16},
          success:function(html){
            $("section article div#morosos").html(html);
            $("section article div#morosos table tr td input#btnauth" ).bind("click",function (){
              do_get_authentication($("section article div#morosos table tr td input#authpasswd").val(),optmenu);
            });
            $("#loading").hide();
          }
  });
}

function do_get_authentication(passphrase,optmenu){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 17,passwd:passphrase},
          success:function(value){
           if (value==0){
					alert("maaaal");
					return false;
			  }	
				else{
					switch(optmenu) {
						case 1: do_get_html_defaulter();
						break;
						case 2: window.location.href="update.php";
						break;
					}
				}	
        $("#loading").hide();
    }
  });
}

function do_get_html_defaulter(){
  $("#loading").show();
  var indexHTML="<div class='dashboard'>\
  <div id='clasificacion'></div>\
  <div id='morosos'></div>\
  </div>";
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 12},
          success:function(html){
            if (!$("section article div.dashboard").length){
              $("section article").html(indexHTML);
              do_get_html_classifications();
            }
            $("section article div#morosos").html(html);
            $("section article div#morosos table tr td.linkimg i" ).bind("click",function (){
              do_update_userstate($(this).attr("id"));
            });
            $("#loading").hide();
          }
  });
}

function do_update_userstate(idlink){
  $("#loading").show();
	var vidlink = idlink.split("_");
	var username="";
	if (vidlink[0]=="del"){
		username=$("div#morosos table tr td.linkimg i#"+idlink).parent().prev().prev().text();
		if (!confirm ("Cepillarse a "+username+" ??" ))
			return false;
	}	
	else{
		username=$("div#morosos table tr td.linkimg i#"+idlink).parent().prev().text();
		if (!confirm ("Seguro que ha pagado "+username+ " ??"))
			return false;

	}	
		
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 13, sausageid:idlink},
          success:function(html){
				$("div#morosos table tr td.linkimg i#"+idlink).parent().parent().remove();
        do_get_html_classifications();
        do_get_html_status();
        $("#loading").hide();
        }
  });
}

function do_get_html_classifications(){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 14},
          success:function(html){
				$("section article div#clasificacion").fadeOut(html);
            $("section article div#clasificacion").html(html);
            $("section article div#clasificacion").fadeIn(html);
            $("section article div#clasificacion table tr td.linkimg i[id^='zoom_']").bind("click",function (){
              var viduser = $(this).attr("id").split("_");
               get_html_ticket(viduser[1]);
             });
            $("#loading").hide();
          }
  });
}

function do_get_html_allgroups(){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 0},
          success:function(html){
            $("section article div#all_groups").html(html);
            $("section article div#all_groups").fadeIn();
            $("#loading").hide();
          }
    });
    
}

function do_get_html_current_userinfo(){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 18},
          success:function(html){
            $("header div#status").html(html);
            $("header div#status").fadeIn();
            $("#loading").hide();
          }
    });
    
}

function do_get_html_status(){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 19},
          success:function(html){
            $("header div#status").html(html);
            $("header div#status").fadeIn();
            $("#loading").hide();
          }
    });
    
}

function do_get_html_phase1(){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 7},
          success:function(html){
            $("section article div#container_phase1 div#matches_phase1").html(html);
            $("section article div#container_phase1").fadeIn();
            $("section article div#container_phase1 input#btn2nextphase" ).bind("click",function (){
              do_get_html_phase2();
            });
            $("#loading").hide();
          }
    });
    
}

function watch_for_duplicated(){
  $("section article div#container_phase2 table#tablephase2 select").bind('change',
    function () {
      let selected = [];
      $("section article div#container_phase2 table#tablephase2 select").each(
        function (){
          if ($(this).val()!=="0"){
            if (selected.find(element => element === $(this).val()))
            { 
              duplicated_name=$(this).find("option:selected").text();
              alert(duplicated_name+" ya lo has seleccionado anteriormente")
              $(this).val("0");
            }
          else
            selected.push($(this).val());
          }
        }
      )
    }
  )
}

function do_get_html_phase2(){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 1},
          success:function(html){
            $("section article div#container_phase1 div.button_bottom_right").remove();
            $("section article div#container_phase2 div#matches_phase2").html(html);
            $("section article div#container_phase2").append(do_get_next());
            $("section article div#container_phase2").fadeIn();
           	$("section article div#container_phase2 input#btn2nextphase" ).bind("click",function (){
              do_get_html_phase3();
            });
            watch_for_duplicated();
            $("#loading").hide();
          }
    });
    
}

function do_get_html_phase3(){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 2},
          success:function(html){
            $("section article div#container_phase2 div.button_bottom_right").remove();
            $("section article div#container_phase3 div#matches_phase3").html(html);
            $("section article div#container_phase3").append(do_get_next());
            $("section article div#container_phase3").fadeIn();
            do_get_allselect_phase3();
            $("section article div#container_phase3 input#btn2nextphase" ).bind("click",function (){
              do_get_html_phase4();
            });
            $("#loading").hide();
          }
    });
    
}

function do_get_allselect_phase3(){
	var html="<select id='win1A2B'><option value='0'>ganador 1A-2B</option>";
	html +="<option value='"+$('#1A').val()+"'>"+$('#1A option:selected').text()+"</option>";
	html +="<option value='"+$('#2B').val()+"'>"+$('#2B option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase3 td#tdwin1A2B").html(html);

	html="<select id='win1B2A'><option value='0'>ganador 1B-2A</option>";
	html +="<option value='"+$('#1B').val()+"'>"+$('#1B option:selected').text()+"</option>";
	html +="<option value='"+$('#2A').val()+"'>"+$('#2A option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase3 td#tdwin1B2A").html(html);

	html="<select id='win1C2D'><option value='0'>ganador 1C-2D</option>";
	html +="<option value='"+$('#1C').val()+"'>"+$('#1C option:selected').text()+"</option>";
	html +="<option value='"+$('#2D').val()+"'>"+$('#2D option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase3 td#tdwin1C2D").html(html);

	html="<select id='win1D2C'><option value='0'>ganador 1D-2C</option>";
	html +="<option value='"+$('#1D').val()+"'>"+$('#1D option:selected').text()+"</option>";
	html +="<option value='"+$('#2C').val()+"'>"+$('#2C option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase3 td#tdwin1D2C").html(html);

	html="<select id='win1E2F'><option value='0'>ganador 1E-2F</option>";
	html +="<option value='"+$('#1E').val()+"'>"+$('#1E option:selected').text()+"</option>";
	html +="<option value='"+$('#2F').val()+"'>"+$('#2F option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase3 td#tdwin1E2F").html(html);

	html="<select id='win1G2H'><option value='0'>ganador 1G-2H</option>";
	html +="<option value='"+$('#1G').val()+"'>"+$('#1G option:selected').text()+"</option>";
	html +="<option value='"+$('#2H').val()+"'>"+$('#2H option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase3 td#tdwin1G2H").html(html);

	html="<select id='win1F2E'><option value='0'>ganador 1F-2E</option>";
	html +="<option value='"+$('#1F').val()+"'>"+$('#1F option:selected').text()+"</option>";
	html +="<option value='"+$('#2E').val()+"'>"+$('#2E option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase3 td#tdwin1F2E").html(html);

	html="<select id='win1H2G'><option value='0'>ganador 1H-2G</option>";
	html +="<option value='"+$('#1H').val()+"'>"+$('#1H option:selected').text()+"</option>";
	html +="<option value='"+$('#2G').val()+"'>"+$('#2G option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase3 td#tdwin1H2G").html(html);
	
	set_bind_change();
}


function do_get_html_phase4(){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 3},
          success:function(html){
            $("section article div#container_phase3 div.button_bottom_right").remove();
            $("section article div#container_phase4 div#matches_phase4").html(html);
            $("section article div#container_phase4").append(do_get_next());
            $("section article div#container_phase4").fadeIn();
            do_get_allselect_phase4();
           	$("section article div#container_phase4 input#btn2nextphase" ).bind("click",function (){
              do_get_html_phase5();
            });
            $("#loading").hide();
          }
    });
    
}

function do_get_allselect_phase4(){
	var html="<select id='win12'><option value='0'>ganador 1-2</option>";
	html +="<option value='"+$('#win1A2B').val()+"'>"+$('#win1A2B option:selected').text()+"</option>";
	html +="<option value='"+$('#win1C2D').val()+"'>"+$('#win1C2D option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase4 td#tdwin12").html(html);

	html="<select id='win56'><option value='0'>ganador 5-6</option>";
	html +="<option value='"+$('#win1E2F').val()+"'>"+$('#win1E2F option:selected').text()+"</option>";
	html +="<option value='"+$('#win1G2H').val()+"'>"+$('#win1G2H option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase4 td#tdwin56").html(html);

	html="<select id='win34'><option value='0'>ganador 3-4</option>";
	html +="<option value='"+$('#win1B2A').val()+"'>"+$('#win1B2A option:selected').text()+"</option>";
	html +="<option value='"+$('#win1D2C').val()+"'>"+$('#win1D2C option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase4 td#tdwin34").html(html);

	html="<select id='win78'><option value='0'>ganador 7-8</option>";
	html +="<option value='"+$('#win1F2E').val()+"'>"+$('#win1F2E option:selected').text()+"</option>";
	html +="<option value='"+$('#win1H2G').val()+"'>"+$('#win1H2G option:selected').text()+"</option>";
	html +="</select>";
	$("table#tablephase4 td#tdwin78").html(html);

	set_bind_change();
}  

function do_get_html_phase5(){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 4},
          success:function(html){
            $("section article div#container_phase4 div.button_bottom_right").remove();
            $("section article div#container_phase5 div#matches_phase5").html(html);
            $("section article div#container_phase5").append(do_get_next());
            do_get_allselect_phase5();
            $("section article div#container_phase5").fadeIn();
             	$("section article div#container_phase5 input#btn2nextphase" ).bind("click",function (){
              do_get_html_champions();
            });
            $("#loading").hide(); 
          }
    });
    
}

function do_get_html_champions(){
  $("#loading").show();
  $.ajax({
          type: 'POST',
          url: "ajax/ajaxhtml.php",
          data: {opt: 5},
          success:function(html){
            $("section article div#container_phase5 div.button_bottom_right").remove();
            $("section article div#container_champions div#champions_results").html(html);
            $("section article div#container_champions").append(do_get_save());
            $("section article div#container_champions").fadeIn();
           	$("section article div#container_champions input#btn2save" ).bind("click",function (){
              do_set_ticket_rappel();
            });
            $("#loading").hide();
          }
    });
    
}

function do_get_allselect_phase5(){
  var html="<select id='winC1C2'><option value='0'>ganador C1-C2</option>";
  html +="<option value='"+$('#win12').val()+"'>"+$('#win12 option:selected').text()+"</option>";
  html +="<option value='"+$('#win56').val()+"'>"+$('#win56 option:selected').text()+"</option>";
  html +="</select>";
  $("table#tablephase5 td#tdwinC1C2").html(html);
  
  html="<select id='winC3C4'><option value='0'>ganador C3-C4</option>";
  html +="<option value='"+$('#win34').val()+"'>"+$('#win34 option:selected').text()+"</option>";
  html +="<option value='"+$('#win78').val()+"'>"+$('#win78 option:selected').text()+"</option>";
  html +="</select>";
  $("table#tablephase5 td#tdwinC3C4").html(html);
  
  set_bind_change();
  
} 

function do_set_ticket_rappel(){
	$("#loading").show();
		
	var Obj1X2=make_json_obj("section article div#container_phase1 select");
	if (!Obj1X2) return false;
	var String1X2 =JSON.stringify(Obj1X2);
	
	var phase2Obj=make_json_obj("section article div#container_phase2 select");
	if (!phase2Obj) return false;
	var phase2String =JSON.stringify(phase2Obj);
	
	var phase3Obj=make_json_obj("section article div#container_phase3 select");
	if (!phase3Obj) return false;
	var phase3String =JSON.stringify(phase3Obj);
	
	var phase4Obj=make_json_obj("section article div#container_phase4 select");
	if (!phase4Obj) return false;
	var phase4String =JSON.stringify(phase4Obj);
	
	var phase5Obj=make_json_obj("section article div#container_phase5 select");
	if (!phase5Obj) return false;
	var phase5String =JSON.stringify(phase5Obj);
	
	if (($("section article div#container_champions input#pichichi").val().length > 0) &&
	 $("section article div#container_champions select#campeon").val()!="0"){
		var phaseChampions={};
		phaseChampions["campeon"]=$("section article div#container_champions select#campeon").val();
		phaseChampions["pichichi"]=$("section article div#container_champions input#pichichi").val();
		var phaseChampionsString =JSON.stringify(phaseChampions);
	}
	else{
		$("section article div#container_champions div#champions_results table#tablechampions td").css("border", "2px solid red");
		return false;
	}	
	
	$.ajax({
			 type: 'POST',
			 url: "ajax/ajaxhtml.php",
			 data: {opt: 6, phase1: String1X2, phase2: phase2String, phase3: phase3String, phase4: phase4String, phase5: phase5String, phaseChampions:phaseChampionsString},
			 success:function(html){
				$("section article").fadeOut();
				$("section article").html(html);
				$("section article").fadeIn();
				$("section article").append(do_get_ok());
        $("#loading").hide();
        $("section article div input#btn2ok[type='button']").bind("click",function (){
				  window.location.href="index.php";
				});
			 }
	 });
}

function get_html_ticket(user){
	$("#loading").show();
	$.ajax({
			 type: 'POST',
			 url: "ajax/ajaxhtml.php",
			 data: {opt: 11, iduser: user},
			 success:function(html){
				$("section article").hide();
				$("section article").html(html);
				$("section article").fadeIn();
				$("section article").append(do_get_index());
        $("#loading").hide();
        $("section article div#btnindex input[type='button']").bind("click",function (){
				  window.location.href="index.php";
				});
			 }
	 });
}
